import {AudioService} from "./AudioService";
import {SoundType} from "./AudioService";

enum WalkDirection {
  LEFT, RIGHT
}

export class Creature {

   private direction: WalkDirection;

   constructor(
      private game: Phaser.Game,
      private sprite: Phaser.Sprite,
      private step: number,
      private audio: AudioService
   ) {
      sprite.animations.add("run");
      sprite.anchor.x = 0.5;
   }

   /*executed on update*/
   public move() {
      this.sprite.animations.play("run", 10, true);
      let isMoveing: boolean = false;
      let playTurn: boolean = false;
      let scaleCoefficient: 1 | -1 = 1;
      if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
         isMoveing = true;
         if (this.direction === WalkDirection.RIGHT) {
            playTurn = true;
         }
         this.sprite.x -= this.step;
      }

      if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
         if (this.direction === WalkDirection.LEFT) {
            scaleCoefficient = -1;
            playTurn = true;
         }
         isMoveing = true;
         this.sprite.x += this.step;
      }

      if (playTurn) {
         this.audio.play(SoundType.turn);
         this.sprite.scale.x *= scaleCoefficient;
      }

      if (isMoveing) {
         this.audio.play(SoundType.walk);
         this.sprite.animations.play("run", 10, true);
      } else {
         this.audio.stop(SoundType.walk);
         this.sprite.animations.stop("run", true);
      }
   }
}