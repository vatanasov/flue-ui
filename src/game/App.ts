
import {Creature} from "./Creature";
import {AudioService} from "./AudioService";

interface Position {
   x: number;
   y: number;
}

interface PersonageDescriptor {
   position: Position;
   spriteKey: string;
}

export class App {

   private game: Phaser.Game;

   private audio: AudioService;

   private readonly WIDTH: number = 640;
   private readonly HEIGHT: number = 640;

   private readonly STEP: number = 4;

   private playerDescriptors: PersonageDescriptor[]= [
      {
         position:{
            x: 10,
            y: 563,
         },
         spriteKey: "mummy"
      },
      {
         position: {
            x: 580,
            y: 548
         },
         spriteKey: "bot"
      }

   ];
   
   private monsters: PersonageDescriptor[] = [{
      position: {
         x: 200,
         y: 403
      },
      spriteKey: "monster"
   }];

   private players: Creature[] = [];

   constructor(private activePlayerIndex: 0 | 1) {
      this.game = new Phaser.Game(
         this.WIDTH, this.HEIGHT,
         Phaser.CANVAS,
         "game", {
            preload: () =>  this.preload(),
            create: () => this.create(),
            update: () => this.update()
         });

      this.audio = new AudioService(this.game);
   }

   private preload() : void {
      this.game.load.tilemap(
         "map",
         "assets/flue-ui.json",
         null,
         Phaser.Tilemap.TILED_JSON
      );

      this.game.load.image(
         "sprite",
         "assets/tiles_spritesheet-x-2.png"
      );

      this.game.load.atlasJSONHash(
         "bot",
         "assets/running_bot.png",
         "assets/running_bot.json"
      );

      this.game.load.spritesheet(
         "mummy",
         "assets/metalslug_mummy37x45.png",
         37,
         45,
         18
      );

      this.game.load.spritesheet(
         "monster",
         "assets/metalslug_monster39x40.png",
         39,
         40
      );

      this.audio.preload();
   }

   private create(): void {
      this.game.physics.startSystem(Phaser.Physics.ARCADE);
      this.game.stage.backgroundColor = "#FEFEFE";

      const map: Phaser.Tilemap = this.game.add.tilemap("map");
      map.addTilesetImage("tiles", "sprite");
      const layer: Phaser.TilemapLayer = map.createLayer("Tile Layer 1");
      layer.resizeWorld();
      this.deployPlayers();
      this.deployMonsters();
      this.audio.create();

      console.log('create');
   }

   private update(): void {
      let player: Creature = this.players[this.activePlayerIndex];
      player.move()
   }

   private deployPlayers(): void {
      for (let pd of this.playerDescriptors) {
         let s: Phaser.Sprite = this.deployPersonage(pd);
         let player: Creature = new Creature(this.game, s, this.STEP, this.audio);
         this.players.push(player);
      }
   }

   private deployPersonage(descriptor: PersonageDescriptor): Phaser.Sprite {
      return this.game.add.sprite(
         descriptor.position.x,
         descriptor.position.y,
         descriptor.spriteKey
      );
   }
   
   private deployMonsters(): void {
      for (let monster of this.monsters) {
         this.deployPersonage(monster);
      }
   }
}