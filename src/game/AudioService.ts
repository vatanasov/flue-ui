export enum SoundType {
   walk =  "walk",
   turn =  "turn",
   shoot =  "shoot"
}

export class AudioService {
   private soundDescriptors: {[name: string]: string[]} = {
      "walk": [
         "assets/sound/walk.mp3",
         "assets/sound/walk.ogg",
      ],
      "turn": [
         "assets/sound/turn.mp3",
         "assets/sound/turn.ogg",
      ],
      "shoot": [
         "assets/sound/apchihu.mp3",
         "assets/sound/apchihu.ogg",
      ]
   };

   private sounds: {[key: string]: Phaser.Sound} = {};

   private playingSounds: SoundType[] = [];

   constructor(private game: Phaser.Game) {

   }

   public preload() {
      for (let name in this.soundDescriptors) {
         this.game.load.audio(name, this.soundDescriptors[name]);
      }
   }

   public create() {
      for (let name in this.soundDescriptors) {
         let sound: Phaser.Sound =
               this.game.add.audio(name, 10, true);
         this.sounds[name] = sound;
      }
   }

   public play(sound: SoundType) {
      this.playInternal(sound);
   }

   public stop(sound: SoundType) {
      this.stopInternal(sound);
   }

   private playInternal(sound: SoundType): void {
      let index: number = this.playingSounds.indexOf(sound);
      if (index === -1) {
         this.sounds[sound].play();
         this.playingSounds.push(sound);
      }
   }

   private stopInternal(sound: SoundType) {
      let index: number = this.playingSounds.indexOf(sound);
      if (index > -1) {
         this.sounds[sound].stop();
         this.playingSounds.splice(index, 1);
      }
   }
}